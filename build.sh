#!/usr/bin/env bash

set -euo pipefail

[[ -z "${BUILD_NUMBER}" ]] && echo "BUILD_NUMBER is required" && exit 1

LUCEE_SHORT_VERSION="5.3"
LUCEE_FULL_VERSION="$(docker run -i --rm lucee/lucee:${LUCEE_SHORT_VERSION} unzip -p /usr/local/tomcat/lucee/lucee.jar lucee/version)"
LUCEE_VERSION="${LUCEE_FULL_VERSION//:[0-9]*/}"

docker build -t "truemark/lucee:${LUCEE_VERSION}-${BUILD_NUMBER}" --build-arg "VERSION=${LUCEE_SHORT_VERSION}" .
docker tag "truemark/lucee:${LUCEE_VERSION}-${BUILD_NUMBER}" "truemark/lucee:${LUCEE_SHORT_VERSION}"
docker push "truemark/lucee:${LUCEE_VERSION}-${BUILD_NUMBER}"
docker push "truemark/lucee:${LUCEE_SHORT_VERSION}"
