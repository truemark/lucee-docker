#!/usr/bin/env bash

# Populate .ssh/authorized_keys from SSH_AUTHORIZED_KEY vars
if [[ ! -r "${HOME}/.ssh/authorized_keys" ]]; then
  KEYS=$(compgen -e | grep SSH_AUTHORIZED_KEY || true)
  if [[ -n "${KEYS}" ]]; then
    echo "Generating ${HOME}/.ssh/authorized_keys"
    mkdir -p "${HOME}/.ssh" && chmod 700 "${HOME}/.ssh"
    for k in ${KEYS}; do echo "${!k}" >> "${HOME}/.ssh/authorized_keys"; done
    chmod 600 "${HOME}/.ssh/authorized_keys"
  fi
else
  echo "Detected existing ${HOME}/.ssh/authorized_keys file"
fi

echo "Generating SSH Host Keys"
ssh-keygen -A

echo "Starting OpenSSH Server"
mkdir -p /run/sshd && /usr/sbin/sshd -e -D &

/opt/td-agent-bit/bin/td-agent-bit -c /etc/td-agent-bit/td-agent-bit.conf &
/usr/local/tomcat/bin/catalina.sh run
