ARG VERSION
FROM lucee/lucee:$VERSION

ENV DEBIAN_FRONTEND=noninteractive

RUN curl -sSL https://packages.fluentbit.io/fluentbit.key | apt-key add - && \
    echo 'deb https://packages.fluentbit.io/debian/buster buster main' >> /etc/apt/sources.list && \
    apt-get -qq update && apt-get -qq upgrade --no-install-recommends && \
    apt-get -qq install imagemagick pdftk vim td-agent-bit openssh-server --no-install-recommends && \
    apt-get -qq clean autoclean && apt-get -qq autoremove && \
    sed -i 's/<policymap>/<policy domain="coder" rights="read | write" pattern="{PDF,TIFF,GIF,JPG,PNG}" \/>\n<policymap>/' /etc/ImageMagick-*/policy.xml && \
    sed -i 's/<policy domain="coder" rights="none" pattern="PDF" \/>/<!-- <policy domain="coder" rights="none" pattern="PDF" \/> -->/g' /etc/ImageMagick-*/policy.xml && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/ && \
    rm -f /usr/local/tomcat/logs/* && \
    sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config && \
    rm -f /etc/ssh/*_key*

COPY overlay/ /
ENTRYPOINT /entrypoint.sh
EXPOSE 8080
EXPOSE 22
