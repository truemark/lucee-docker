# TrueMark DAPPL Lucee Runtime Docker

## Production Environment

The following sysctl.conf settings are recommended
```
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 128
fs.inotify.max_user_watches = 524288
```

## Links
 - [Lucee Docker GitHub](https://github.com/lucee/lucee-dockerfiles)

# 27-Jul-2021 18:23:25.280
